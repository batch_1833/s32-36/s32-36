const Course = require("../models/Course");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body.
	2. Save the new User to the database.

*/

module.exports.addCourse = (reqBody) =>{

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	})

	return newCourse.save().then((course, error) =>{
		if(error){
			return false
		}
		else{
			return true
		}
	})
}

// Retrieve all courses
/*
	Step all courses

	Step:
	1. Retrieve all the courses from the database
*/

module.exports.getAllCourses = () =>{
	return Course.find({}).then(result => result);
}

// Retrieve all Active Courses
/*
	Steps : 
	1. Retrieve all the courses from the database with the property of "isActive" to true.

*/
module.exports.getAllActive = () =>{
	return Course.find({isActive: true}).then(result =>result);
}

// Retrieving a specific course
/*
	Step:
	1. Retrieve the course that matches the courseId provided from the URL

*/
module.exports.getCourse =(courseId) =>{
	return Course.findById(courseId).then(result => result)
}

// Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body.
	2. Find and update the course useing the courseId retrieved from rquest paramas/url property and the variable "updatedCourse" containing the information from the request body

*/

module.exports.updateCourse = (courseId, reqBody) => {
	// Specify the fields/properties to be updated
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	}

	// Syntax: findByIdAndUpdate(documentId, updatesToBeApplied)

	return Course.findByIdAndUpdate(courseId, updatedCourse).then((courseUpdate, error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// Archive a course
/*
	Mini Activity:
	1. Create a "archiveCourse" function that will change the status of an active course to inactive using the "isActive" property.
	2. Once done, send a screenshot of your postman in the batch hangouts.

*/

module.exports.archiveCourse = (courseId, reqBody)=>{

	let updatedStatus = {
		isActive: reqBody.isActive	
	}

	return Course.findByIdAndUpdate(courseId, updatedStatus).then((statusUpdate, error) =>{
		if(error){
			return false;
		}
		else{
			return true
		}
	})
}